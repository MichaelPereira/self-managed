# Self Hosted

This projects contains configuration to setup a self-hosted instance of R2devops.

You can check the complete documentation [here](https://docs.r2devops.io/self-hosted-requirements/)
